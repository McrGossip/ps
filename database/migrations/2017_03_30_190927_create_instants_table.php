<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instants', function (Blueprint $table) {
            $table->increments('id');
			$table->string('employee_name');
			$table->integer('till_1_start');
			$table->integer('till_2_start');
			$table->integer('game_1_start');
			$table->integer('game_2_start');
			$table->integer('game_3_start');
			$table->integer('game_4_start');
			$table->integer('game_5_start');
			$table->integer('game_6_start');
			$table->integer('game_7_start');
			$table->integer('game_8_start');
			$table->integer('game_9_start');
			$table->integer('game_10_start');
			$table->integer('game_11_start');
			$table->integer('game_12_start');
			$table->integer('game_13_start');
			$table->integer('game_14_start');
			$table->integer('game_15_start');
            $table->integer('game_16_start');
			$table->integer('game_17_start');
			$table->integer('game_18_start');
			$table->integer('game_19_start');
			$table->integer('game_20_start');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instants');
    }
}
