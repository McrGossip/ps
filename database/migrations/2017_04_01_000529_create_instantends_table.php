<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstantendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instantends', function (Blueprint $table) {
            $table->increments('id');
			$table->string('employee_name');
			$table->integer('till_1_end');
			$table->integer('till_2_end');
			$table->integer('game_1_end');
			$table->integer('game_2_end');
			$table->integer('game_3_end');
			$table->integer('game_4_end');
			$table->integer('game_5_end');
			$table->integer('game_6_end');
			$table->integer('game_7_end');
			$table->integer('game_8_end');
			$table->integer('game_9_end');
			$table->integer('game_10_end');
			$table->integer('game_11_end');
			$table->integer('game_12_end');
			$table->integer('game_13_end');
			$table->integer('game_14_end');
			$table->integer('game_15_end');
			$table->integer('game_16_end');
			$table->integer('game_17_end');
			$table->integer('game_18_end');
			$table->integer('game_19_end');
			$table->integer('game_20_end');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instantends');
    }
}
