<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotteriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotteries', function (Blueprint $table) {
			$table->increments('id');
            $table->string('employee_name');
			$table->integer('lm_1');
			$table->integer('lm_2');
			$table->integer('lm_3');
			$table->integer('lm_4');
			$table->integer('lm_5');
			$table->integer('lm_6');
			$table->integer('lm_7');
			$table->integer('lm_8');
			$table->integer('lm_9');
			$table->integer('lm_10');
			$table->integer('lm_11');
			$table->integer('till_1_1');
			$table->integer('till_1_2');
			$table->integer('till_1_3');
			$table->integer('till_1_4');
			$table->integer('till_2_1');
			$table->integer('till_2_2');
			$table->integer('till_2_3');
			$table->integer('till_2_4');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotteries');
    }
}
