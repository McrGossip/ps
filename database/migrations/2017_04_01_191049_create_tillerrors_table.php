<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTillerrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tillerrors', function (Blueprint $table) {
            $table->increments('id');
			$table->string('employee_name'); // employee
			$table->integer('till_1_1'); // till 1 input 1
			$table->integer('till_1_2'); // till 1 input 2
			$table->integer('till_1_3'); // till 1 input 3
			$table->integer('till_1_4'); // till 1 input 4
			$table->integer('till_1_5'); // till 1 input 5
			$table->integer('till_1_6'); // till 1 input 6
			$table->integer('till_1_7'); // till 1 input 7
			$table->text('till_1_note'); // Notes for table 1
			$table->integer('till_2_1'); // till 2 input 1
			$table->integer('till_2_2'); // till 2 input 2
			$table->integer('till_2_3'); // till 2 input 3
			$table->integer('till_2_4'); // till 2 input 4
			$table->integer('till_2_5'); // till 2 input 5
			$table->integer('till_2_6'); // till 2 input 6
			$table->integer('till_2_7'); // till 2 input 7
			$table->text('till_2_note'); // Notes for table 2
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tillerrors');
    }
}
