<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayzonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payzones', function (Blueprint $table) {
            $table->increments('id');
			$table->string('employee_name');
			$table->integer('till_1_start');
			$table->integer('till_1_end');
			$table->integer('till_2_start');
			$table->integer('till_2_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payzones');
    }
}
