<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::resource('instant', 'InstantController');
Route::resource('instantend', 'InstantendController');
Route::resource('lottery', 'LotteryController');
Route::resource('payzone', 'PayzoneController');
Route::resource('till_errors', 'TillErrorsController');

Route::resource('games', 'GamesController');
