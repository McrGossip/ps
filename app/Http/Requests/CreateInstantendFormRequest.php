<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateInstantendFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name' => 'required',
            'till_1_end' => 'required',
            'till_2_end' => 'required',
            'game_1_end' => 'required',
            'game_2_end' => 'required',
            'game_3_end' => 'required',
            'game_4_end' => 'required',
            'game_5_end' => 'required',
            'game_6_end' => 'required',
            'game_7_end' => 'required',
            'game_8_end' => 'required',
            'game_9_end' => 'required',
            'game_10_end' => 'required',
            'game_11_end' => 'required',
            'game_12_end' => 'required',
            'game_13_end' => 'required',
            'game_14_end' => 'required',
            'game_15_end' => 'required',
            'game_16_end' => 'required',
            'game_17_end' => 'required',
            'game_18_end' => 'required',
            'game_19_end' => 'required',
            'game_20_end' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'employee_name.required' => 'Please include your name',
            'till_1_end.required'  => 'Till 1 end figure is missing',
            'till_2_end.required'  => 'Till 2 end figure is missing'
        ];
    }
}
