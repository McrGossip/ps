<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateInstantFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name' => 'required',
            'till_1_start' => 'required',
            'till_2_start' => 'required',
            'game_1_start' => 'required',
            'game_2_start' => 'required',
            'game_3_start' => 'required',
            'game_4_start' => 'required',
            'game_5_start' => 'required',
            'game_6_start' => 'required',
            'game_7_start' => 'required',
            'game_8_start' => 'required',
            'game_9_start' => 'required',
            'game_10_start' => 'required',
            'game_11_start' => 'required',
            'game_12_start' => 'required',
            'game_13_start' => 'required',
            'game_14_start' => 'required',
            'game_15_start' => 'required',
            'game_16_start' => 'required',
            'game_17_start' => 'required',
            'game_18_start' => 'required',
            'game_19_start' => 'required',
            'game_20_start' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'employee_name.required' => 'Forgot to put your name in stupid',
            'till_1_start.required'  => 'Till 1 start figure is missing',
            'till_2_start.required'  => 'Till 2 start figure is missing'
        ];
    }
}
