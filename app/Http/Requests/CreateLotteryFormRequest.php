<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateLotteryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name' => 'required',
            'till_1_1' => 'required',
            'till_1_2' => 'required',
            'till_1_3' => 'required',
            'till_1_4' => 'required',
            'till_2_1' => 'required',
            'till_2_2' => 'required',
            'till_2_3' => 'required',
            'till_2_4' => 'required',
            'lm_1' => 'required',
            'lm_2' => 'required',
            'lm_3' => 'required',
            'lm_4' => 'required',
            'lm_5' => 'required',
            'lm_6' => 'required',
            'lm_7' => 'required',
            'lm_8' => 'required',
            'lm_9' => 'required',
            'lm_10' => 'required',
            'lm_11' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'employee_name.required' => 'Forgot to put your name in stupid',
            'till_1_1.required'  => 'Till 1 figure 1 is missing',
            'till_1_2.required'  => 'Till 1 figure 2 is missing'
        ];
    }
}
