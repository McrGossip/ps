<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateGamesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
       
            'till_1' => 'required',
            'till_2' => 'required',
        ];
    }

    public function messages()
    {
        return [ 
            'till_1.required'  => 'Till 1 start figure is missing',
            'till_2.required'  => 'Till 2 start figure is missing'
        ];
    }
}
