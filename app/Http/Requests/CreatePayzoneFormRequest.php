<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePayzoneFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name' => 'required',
            'till_1_start' => 'required',
            'till_1_end' => 'required',
            'till_2_start' => 'required',
            'till_2_end' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'employee_name.required' => 'Please include your name',
            'till_1_start.required'  => 'Till 1 start figure is missing',
            'till_1_end.required'  => 'Till 1 end figure is missing',
            'till_2_start.required'  => 'Till 2 start figure is missing',
            'till_2_end.required'  => 'Till 2 end figure is missing'
        ];
    }
}
