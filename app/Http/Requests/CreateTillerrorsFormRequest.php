<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTillerrorsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name' => 'required',
            'till_1_1' => 'required',
            'till_1_2' => 'required',
            'till_1_3' => 'required',
            'till_1_4' => 'required',
            'till_1_5' => 'required',
            'till_1_6' => 'required',
            'till_1_7' => 'required',
            'till_2_1' => 'required',
            'till_2_2' => 'required',
            'till_2_3' => 'required',
            'till_2_4' => 'required',
            'till_2_5' => 'required',
            'till_2_6' => 'required',
            'till_2_7' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'employee_name.required' => 'Please include your name',
            'till_1_1.required'  => 'Till 1 figure 1 is missing',
            'till_1_2.required'  => 'Till 1 figure 2 is missing',
            'till_2_1.required'  => 'Till 2 figure 1 is missing',
            'till_2_2.required'  => 'Till 2 figure 2 is missing'
        ];
    }
}
