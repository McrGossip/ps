<?php namespace

App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instantend;
use App\Http\Requests\CreateInstantendFormRequest;

class InstantendController extends Controller
{	
 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		return redirect('instantend/create');
	}

	public function create(){
	  return view('forms.instantend.create');
	}
	

	public function store(CreateInstantendFormRequest $request){
		
		$instantend = new Instantend;
		$instantend->id;
		$instantend->employee_name = $request->get('employee_name');
		$instantend->till_1_end = $request->get('till_1_end');
		$instantend->till_2_end = $request->get('till_2_end');
		$instantend->game_1_end = $request->get('game_1_end');
		$instantend->game_2_end = $request->get('game_2_end');
		$instantend->game_3_end = $request->get('game_3_end');
		$instantend->game_4_end = $request->get('game_4_end');
		$instantend->game_5_end = $request->get('game_5_end');
		$instantend->game_6_end = $request->get('game_6_end');
		$instantend->game_7_end = $request->get('game_7_end');
		$instantend->game_8_end = $request->get('game_8_end');
		$instantend->game_9_end = $request->get('game_9_end');
		$instantend->game_10_end = $request->get('game_10_end');
		$instantend->game_11_end = $request->get('game_11_end');
		$instantend->game_12_end = $request->get('game_12_end');
		$instantend->game_13_end = $request->get('game_13_end');
		$instantend->game_14_end = $request->get('game_14_end');
		$instantend->game_15_end = $request->get('game_15_end');
		$instantend->game_16_end = $request->get('game_16_end');
		$instantend->game_17_end = $request->get('game_17_end');
		$instantend->game_18_end = $request->get('game_18_end');
		$instantend->game_19_end = $request->get('game_19_end');
		$instantend->game_20_end = $request->get('game_20_end');
		$instantend->save();
	
		return \Redirect::route('instantend.show', array($instantend->id))
			->with('message', 'Nice work!');
	}
	
	public function show(){
	  return view('forms.instantend.show');
	}
	
}
