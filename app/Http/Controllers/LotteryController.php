<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lottery;
use App\Http\Requests\CreateLotteryFormRequest;

class LotteryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('lottery/create');
    }
    
    public function create(){
        return view('forms.lottery.create');
    }

    public function store(CreateLotteryFormRequest $request){

        $lottery = new Lottery;
        $lottery->id;
        $lottery->employee_name = $request->get('employee_name');
        $lottery->till_1_1 = $request->get('till_1_1');
        $lottery->till_1_2 = $request->get('till_1_2');
        $lottery->till_1_3 = $request->get('till_1_3');
        $lottery->till_1_4 = $request->get('till_1_4');
        $lottery->till_2_1 = $request->get('till_2_1');
        $lottery->till_2_2 = $request->get('till_2_2');
        $lottery->till_2_3 = $request->get('till_2_3');
        $lottery->till_2_4 = $request->get('till_2_4');
        $lottery->lm_1 = $request->get('lm_1');
        $lottery->lm_2 = $request->get('lm_2');
        $lottery->lm_3 = $request->get('lm_3');
        $lottery->lm_4 = $request->get('lm_4');
        $lottery->lm_5 = $request->get('lm_5');
        $lottery->lm_6 = $request->get('lm_6');
        $lottery->lm_7 = $request->get('lm_7');
        $lottery->lm_8 = $request->get('lm_8');
        $lottery->lm_9 = $request->get('lm_9');
        $lottery->lm_10 = $request->get('lm_10');
        $lottery->lm_11 = $request->get('lm_11');
        $lottery->save();

        return \Redirect::route('lottery.show', array($lottery->id))
            ->with('message', 'Nice work!');
    }

    public function show(){
        return view('forms.lottery.show');
    }

}
