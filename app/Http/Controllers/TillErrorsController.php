<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tillerrors;
use App\Http\Requests\CreateTillerrorsFormRequest;

class TillErrorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		return redirect('till_errors/create');
	}

   	public function create(){
      return view('forms.till_errors.create');
	}

	public function store(CreateTillerrorsFormRequest $request){
       
	    $tillerrors = new Tillerrors;
		$tillerrors->id;
		$tillerrors->employee_name = $request->get('employee_name');
		$tillerrors->till_1_1 = $request->get('till_1_1');
		$tillerrors->till_1_2 = $request->get('till_1_2');
		$tillerrors->till_1_3 = $request->get('till_1_3');
		$tillerrors->till_1_4 = $request->get('till_1_4');
		$tillerrors->till_1_5 = $request->get('till_1_5');
		$tillerrors->till_1_6 = $request->get('till_1_6');
		$tillerrors->till_1_7 = $request->get('till_1_7');
		$tillerrors->till_2_1 = $request->get('till_2_1');
		$tillerrors->till_2_2 = $request->get('till_2_2');
		$tillerrors->till_2_3 = $request->get('till_2_3');
		$tillerrors->till_2_4 = $request->get('till_2_4');
		$tillerrors->till_2_5 = $request->get('till_2_5');
		$tillerrors->till_2_6 = $request->get('till_2_6');
		$tillerrors->till_2_7 = $request->get('till_2_7');
		$tillerrors->till_1_note = $request->get('till_1_note');
		$tillerrors->till_2_note = $request->get('till_2_note');
		$tillerrors->save();
	
		return \Redirect::route('till_errors.create', 
			array($tillerrors->id))
			->with('message', 'Nice work!');
	}
	
	
}
