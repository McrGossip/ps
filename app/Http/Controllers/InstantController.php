<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instant;
use App\Http\Requests\CreateInstantFormRequest;


class InstantController extends Controller
{	
 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		return redirect('instant/create');
	}

	public function create(){
	  return view('forms.instant.create');
	}
	

	public function store(CreateInstantFormRequest $request){
		
		$instant = new Instant;
		$instant->id;
		$instant->employee_name = $request->get('employee_name');
		$instant->till_1_start = $request->get('till_1_start');
		$instant->till_2_start = $request->get('till_2_start');
		$instant->game_1_start = $request->get('game_1_start');
		$instant->game_2_start = $request->get('game_2_start');
		$instant->game_3_start = $request->get('game_3_start');
		$instant->game_4_start = $request->get('game_4_start');
		$instant->game_5_start = $request->get('game_5_start');
		$instant->game_6_start = $request->get('game_6_start');
		$instant->game_7_start = $request->get('game_7_start');
		$instant->game_8_start = $request->get('game_8_start');
		$instant->game_9_start = $request->get('game_9_start');
		$instant->game_10_start = $request->get('game_10_start');
		$instant->game_11_start = $request->get('game_11_start');
		$instant->game_12_start = $request->get('game_12_start');
		$instant->game_13_start = $request->get('game_13_start');
		$instant->game_14_start = $request->get('game_14_start');
		$instant->game_15_start = $request->get('game_15_start');
		$instant->game_16_start = $request->get('game_16_start');
		$instant->game_17_start = $request->get('game_17_start');
		$instant->game_18_start = $request->get('game_18_start');
		$instant->game_19_start = $request->get('game_19_start');
		$instant->game_20_start = $request->get('game_20_start');
		$instant->save();
	
		return \Redirect::route('instant.show', array($instant->id))
			->with('message', 'Nice work!');
	}
	
	public function show(){
	  return view('forms.instant.show');
	}
	
}
