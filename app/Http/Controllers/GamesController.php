<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Games;
use App\Http\Requests\CreateGamesFormRequest;

class GamesController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect('games/create');
    }

    public function create(){
      return view('forms.games.create');
    }

   
    

    public function store(CreateGamesFormRequest $request){
        
        $games = new Games;
        $games->id;
        // $games->employee_name = $name;
        $games->game_number = $request->get('game_number');
        $games->e = $request->get('e');
        $games->till_2 = $request->get('till_2');
        $games->till_1 = $request->get('till_1');
        $games->start = $request->get('start');
        $games->to = $request->get('to');
        $games->save();
    
        return \Redirect::route('games.show', array($games->id))
            ->with('message', 'Nice work!');
    }
    
    public function show(){
      return view('forms.games.show');
    }
    
}
