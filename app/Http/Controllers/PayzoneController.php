<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payzone;
use App\Http\Requests\CreatePayzoneFormRequest;
use Alert;

class PayzoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('payzone/create');
    }

    public function create(){
        return view('forms.payzone.create');
    }

    public function store(CreatePayzoneFormRequest $request){

        $payzone = new Payzone;
        $payzone->id;
        $payzone->employee_name = $request->get('employee_name');
        $payzone->till_1_start = $request->get('till_1_start');
        $payzone->till_1_end = $request->get('till_1_end');
        $payzone->till_2_start = $request->get('till_2_start');
        $payzone->till_2_end = $request->get('till_2_end');
        $payzone->save();

        return \Redirect::route('payzone.show', array($payzone->id))
            ->with('message', 'Nice work!');
    }

    public function show(){
        return view('forms.payzone.show');
        alert()->success('Success Message', 'Optional Title');
    }

}
