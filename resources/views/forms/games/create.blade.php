@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="panel panel-default">
            
            <h1 style="text-align: center">Games </h1>

{!! Form::open(
  array(
    'route' => 'games.store', 
    'class' => 'form')
  ) !!}

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <div><li> {{ $error }}</li></div>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
          Game #  {!! Form::selectRange('game_number', 1, 20, 
                array('placeholder'=>'game #')) !!}
            </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            {!!  Form::select('price', array('L' => '10', 'S' => '5'), '10') !!}
            </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            {!! Form::text('start', null,
                array('class'=>'form-control','placeholder'=>'Start')) !!}
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-3">
                {!! Form::text('to', null,
                array('class'=>'form-control','placeholder'=>'To')) !!}
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                {!! Form::checkbox('e') !!} new pack? 
        </div>
    </div>
    <div class="form-group">


        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                {!! Form::number('till_1', null, 
                array('class'=>'form-control','placeholder'=>'Till 1')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                {!! Form::number('till_2', null, 
                array('class'=>'form-control','placeholder'=>'Till 2')) !!}
        </div>
    </div>
    </div>
</div>

<div class="form-group" style="margin-bottom: 40px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    {!! Form::submit('Create Games!', 
      array('class'=>'btn btn-primary'
    )) !!}
        </div>
</div>
{!! Form::close() !!}
</div>
            
			</div>
    </div>
</div>
@endsection
