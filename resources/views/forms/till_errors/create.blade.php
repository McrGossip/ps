@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">

                <h1 style="text-align: center">Till errors</h1>

                {!! Form::open(
                  array(
                    'route' => 'till_errors.store',
                    'class' => 'form')
                  ) !!}

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div><li> {{ $error }}</li></div>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::text('employee_name', null,
                            array('class'=>'form-control','placeholder'=>'Employee Name')) !!}
                        </div>
                        <h2 style="text-align: center">Till 1</h2>
                        
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_1', null,
                                array('class'=>'form-control','placeholder'=>'Figure 1')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_2', null,
                                array('class'=>'form-control','placeholder'=>'Figure 2')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_3', null,
                                array('class'=>'form-control','placeholder'=>'Figure 3')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_4', null,
                                array('class'=>'form-control','placeholder'=>'Figure 4')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_5', null,
                                array('class'=>'form-control','placeholder'=>'Figure 5')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_6', null,
                                array('class'=>'form-control','placeholder'=>'Figure 6')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_7', null,
                                array('class'=>'form-control','placeholder'=>'Figure 7')) !!}
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::text('till_1_note', null,
                                array('class'=>'form-control','placeholder'=>'Till 1 Notes')) !!}
                        </div>


                        <hr>
                        <h2 style="text-align: center" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Till 2</h2>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_1', null,
                                array('class'=>'form-control','placeholder'=>'Figure 1')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_2', null,
                                array('class'=>'form-control','placeholder'=>'Figure 2')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_3', null,
                                array('class'=>'form-control','placeholder'=>'Figure 3')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_4', null,
                                array('class'=>'form-control','placeholder'=>'Figure 4')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_5', null,
                                array('class'=>'form-control','placeholder'=>'Figure 5')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_6', null,
                                array('class'=>'form-control','placeholder'=>'Figure 6')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_7', null,
                                array('class'=>'form-control','placeholder'=>'Figure 7')) !!}
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::text('till_2_note', null,
                                array('class'=>'form-control','placeholder'=>'Till 2 Notes')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 40px;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {!! Form::submit('Submit Till Errors',
                          array('class'=>'btn btn-primary'
                        )) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
