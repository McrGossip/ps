@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">

                <h1 style="text-align: center">Lottery</h1>

                {!! Form::open(
                  array(
                    'route' => 'lottery.store',
                    'class' => 'form')
                  ) !!}

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div><li> {{ $error }}</li></div>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::text('employee_name', null,
                            array('class'=>'form-control','placeholder'=>'Employee Name')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_1', null,
                                array('class'=>'form-control','placeholder'=>'Till One 1')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_2', null,
                                array('class'=>'form-control','placeholder'=>'Till One 2')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_3', null,
                                array('class'=>'form-control','placeholder'=>'Till One 3')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_4', null,
                                array('class'=>'form-control','placeholder'=>'Till One 4')) !!}
                        </div>

                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_1', null,
                                array('class'=>'form-control','placeholder'=>'Till Two 1')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_2', null,
                                array('class'=>'form-control','placeholder'=>'Till Two 2')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_3', null,
                                array('class'=>'form-control','placeholder'=>'Till Two 3')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_4', null,
                                array('class'=>'form-control','placeholder'=>'Till Two 4')) !!}
                        </div>

                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_1', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 1')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_2', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 2')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_3', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 3')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_4', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 4')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_5', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 5')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_6', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 6')) !!}
                        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_7', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 7')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_8', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 8')) !!}
                        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_9', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 9')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_10', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 10')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                            {!! Form::text('lm_11', null,
                                array('class'=>'form-control','placeholder'=>'Lottery 11')) !!}
                        </div>

                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 40px;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {!! Form::submit('Create Lottery!',
                          array('class'=>'btn btn-primary'
                        )) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
