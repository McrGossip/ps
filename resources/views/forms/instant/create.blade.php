@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="panel panel-default">
            
            <h1 style="text-align: center">Instant <small>(Start of shift)</small></h1>

{!! Form::open(
  array(
    'route' => 'instant.store', 
    'class' => 'form')
  ) !!}

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <div><li> {{ $error }}</li></div>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::text('employee_name', null,
    	array('class'=>'form-control','placeholder'=>'Employee Name')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('till_1_start', null, 
    	array('class'=>'form-control','placeholder'=>'Till 1 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('till_2_start', null, 
    	array('class'=>'form-control','placeholder'=>'Till 2 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_1_start', null,
                array('class'=>'form-control','placeholder'=>'Game 1 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                {!! Form::text('game_2_start', null,
                    array('class'=>'form-control','placeholder'=>'Game 2 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_3_start', null,
                array('class'=>'form-control','placeholder'=>'Game 3 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_4_start', null,
                array('class'=>'form-control','placeholder'=>'Game 4 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_5_start', null,
                array('class'=>'form-control','placeholder'=>'Game 5 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_6_start', null,
                array('class'=>'form-control','placeholder'=>'Game 6 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_7_start', null,
                array('class'=>'form-control','placeholder'=>'Game 7 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_8_start', null,
                array('class'=>'form-control','placeholder'=>'Game 8 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_9_start', null,
                array('class'=>'form-control','placeholder'=>'Game 9 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_10_start', null,
                array('class'=>'form-control','placeholder'=>'Game 10 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_11_start', null,
                array('class'=>'form-control','placeholder'=>'Game 11 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_12_start', null,
                array('class'=>'form-control','placeholder'=>'Game 12 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_13_start', null,
                array('class'=>'form-control','placeholder'=>'Game 13 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_14_start', null,
                array('class'=>'form-control','placeholder'=>'Game 14 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_15_start', null,
                array('class'=>'form-control','placeholder'=>'Game 15 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_16_start', null,
                array('class'=>'form-control','placeholder'=>'Game 16 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_17_start', null,
                array('class'=>'form-control','placeholder'=>'Game 17 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_18_start', null,
                array('class'=>'form-control','placeholder'=>'Game 18 Start')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_19_start', null,
                array('class'=>'form-control','placeholder'=>'Game 19 Start')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_20_start', null,
                array('class'=>'form-control','placeholder'=>'Game 20 Start')) !!}
        </div>



    </div>
</div>

<div class="form-group" style="margin-bottom: 40px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    {!! Form::submit('Create Instant!', 
      array('class'=>'btn btn-primary'
    )) !!}
        </div>
</div>
{!! Form::close() !!}
</div>
            
			</div>
    </div>
</div>
@endsection
