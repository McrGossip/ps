@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="panel panel-default">
            
            <h1 style="text-align: center">Instant <small>(End of shift)</small></h1>

{!! Form::open(
  array(
    'route' => 'instantend.store',
    'class' => 'form')
  ) !!}

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <div><li> {{ $error }}</li></div>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::text('employee_name', null,
    	array('class'=>'form-control','placeholder'=>'Employee Name')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('till_1_end', null,
    	array('class'=>'form-control','placeholder'=>'Till One End')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('till_2_end', null,
    	array('class'=>'form-control','placeholder'=>'Till Two End')) !!}
            </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_1_end', null,
                array('class'=>'form-control','placeholder'=>'Game 1 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_2_end', null,
                array('class'=>'form-control','placeholder'=>'Game 2 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_3_end', null,
                array('class'=>'form-control','placeholder'=>'Game 3 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_4_end', null,
                array('class'=>'form-control','placeholder'=>'Game 4 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_5_end', null,
                array('class'=>'form-control','placeholder'=>'Game 5 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_6_end', null,
                array('class'=>'form-control','placeholder'=>'Game 6 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_7_end', null,
                array('class'=>'form-control','placeholder'=>'Game 7 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_8_end', null,
                array('class'=>'form-control','placeholder'=>'Game 8 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_9_end', null,
                array('class'=>'form-control','placeholder'=>'Game 9 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_10_end', null,
                array('class'=>'form-control','placeholder'=>'Game 10 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_11_end', null,
                array('class'=>'form-control','placeholder'=>'Game 11 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_12_end', null,
                array('class'=>'form-control','placeholder'=>'Game 12 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_13_end', null,
                array('class'=>'form-control','placeholder'=>'Game 13 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_14_end', null,
                array('class'=>'form-control','placeholder'=>'Game 14 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_15_end', null,
                array('class'=>'form-control','placeholder'=>'Game 15 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_16_end', null,
                array('class'=>'form-control','placeholder'=>'Game 16 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_17_end', null,
                array('class'=>'form-control','placeholder'=>'Game 17 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_18_end', null,
                array('class'=>'form-control','placeholder'=>'Game 18 End')) !!}
        </div> <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_19_end', null,
                array('class'=>'form-control','placeholder'=>'Game 19 End')) !!}
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {!! Form::text('game_20_end', null,
                array('class'=>'form-control','placeholder'=>'Game 20 End')) !!}
        </div>

    </div>
</div>

<div class="form-group" style="margin-bottom: 40px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    {!! Form::submit('Create Instant End!',
      array('class'=>'btn btn-primary'
    )) !!}
        </div>
</div>
{!! Form::close() !!}
</div>
            
			</div>
    </div>
</div>
@endsection
