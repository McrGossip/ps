@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">

                <h1 style="text-align: center">Payzone</h1>

                {!! Form::open(
                  array(
                    'route' => 'payzone.store',
                    'class' => 'form')
                  ) !!}

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div><li> {{ $error }}</li></div>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::text('employee_name', null,
                            array('class'=>'form-control','placeholder'=>'Employee Name')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_start', null,
                                array('class'=>'form-control','placeholder'=>'Till 1 start')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_1_end', null,
                                array('class'=>'form-control','placeholder'=>'Till 1 end')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_start', null,
                                array('class'=>'form-control','placeholder'=>'Till 2 start')) !!}
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::text('till_2_end', null,
                                array('class'=>'form-control','placeholder'=>'Till 2 end')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 40px;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {!! Form::submit('Submit Payzone',
                          array('class'=>'btn btn-primary'
                        )) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
