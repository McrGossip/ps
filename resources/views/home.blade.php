@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Forms ~ Dashboard</div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <a href="games/create" class="btn btn-info btn-lg" role="button" style="width: 100%; margin-bottom: 20px" >Games</a>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                <a href="instant/create" class="btn btn-info btn-lg" role="button" style="width: 100%; margin-bottom: 20px" >Instant<br><small>(start of shift)</small></a>
                            </div><div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                <a href="instantend/create" class="btn btn-default btn-lg" style="width: 100%; margin-bottom: 20px" role="button">Instant<br><small>(end of shift)</small></a>
                            </div><div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
                                <a href="lottery/create" class="btn btn-success btn-lg" style="width: 100%; margin-bottom: 20px" role="button">Lottery</a>
                            </div><div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
                                <a href="payzone/create" class="btn btn-primary btn-lg" style="width: 100%; margin-bottom: 20px" role="button">Payzone</a>
                            </div><div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
                                <a href="till_errors/create" class="btn btn-warning btn-lg" style="width: 100%;margin-bottom: 20px" role="button">Till Errors</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
