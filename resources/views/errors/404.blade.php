@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h1 style="font-size:48px">404 Page not found</h1> </div>

                    <div class="panel-body">
                        <a href="/home" class="btn btn-info btn-lg" role="button">Home</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection